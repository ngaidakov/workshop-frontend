import React from 'react'
import ReactDOM from 'react-dom/client'
import SlidesTesting from './SlidesTesting.tsx'

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <SlidesTesting />
  </React.StrictMode>,
)
