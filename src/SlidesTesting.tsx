import {
  Appear, Box,
  Deck, FullScreen,
  Heading,
  Image, ListItem, Notes, Slide,
  SlideLayout, SpectacleThemeOverrides, UnorderedList, useSteps,
} from 'spectacle'
import twConfig from '../tailwind.config.ts'
import resolveConfig from 'tailwindcss/resolveConfig'
import { AppearList, AppearReplace, Code, Tree, TwoColumn } from './slideUtils.tsx'

import imgJest from './assets/jest.svg'
import imgVue from './assets/vue.svg'
import imgTestResults from './assets/test-results.png'

const tw = resolveConfig(twConfig).theme
const colors = tw.colors

const theme: SpectacleThemeOverrides = {
  fonts: {
    header: tw.fontFamily.body,
    text: tw.fontFamily.body,
    monospace: tw.fontFamily.mono,
  },
  colors: {
    primary: colors.neutral['300'],
    secondary: colors.yellow['500'],
    tertiary: colors.neutral['800'],
    quaternary: colors.neutral['100'],
    quinary: colors.yellow['600'],
  },
  fontSizes: {
    monospace: '1.5rem',
  },
}

export default function SlidesTesting () {
  return (
    <Deck theme={theme} template={<Box className="absolute left-8 bottom-8 cursor-pointer"><FullScreen /></Box>}>
      <SlideLayout.Center>
        <Heading>Unit testing Vue components</Heading>
      </SlideLayout.Center>

      <TwoColumn
        id="logos"
        center
        equal
        left={<Image src={imgJest} alt="Jest logo" className="w-4/5" />}
        right={<>
          <Image src={imgVue} alt="Vue logo" className="w-2/5" />
          <Heading className="!text-neutral-200">Vue Test Utils</Heading>
        </>}
      />

      <TwoColumn
        id="jest-describe"
        center
        left={<Image src={imgJest} alt="Jest logo" className="w-72" />}
        right={<Box className="flex gap-6">
          <Appear><Code text="sm">{`
describe('PopoverButton', () => {
  beforeAll(() => {
    // ...
  })

  beforeEach(() => {
    // ...
  })

  it('renders text in button', () => {
    // ...
    expect(someValue).toBe('test')
  })

  test('popover opens when button is clicked', () => {
    // ...
  })

  describe('Podducks rain down', () => {
    beforeEach(() => {
      // ...
    })

    it('starts raining', () => {
      // ...
    })

    it('stops raining eventually', () => {
      // ...
    })
  })
})
          `}</Code></Appear>

          <Appear className="max-h-max">
            <Image src={imgTestResults} alt="Test results" className="object-contain" />
          </Appear>
        </Box>}
      />

      <SlideLayout.Center>
        <Image src={imgVue} alt="Vue logo" className="w-72" />
      </SlideLayout.Center>

      <SlideLayout.Center id="what-to-test">
        <Heading>What to test?</Heading>
        <AppearList className="!text-xl !leading-tight">
          <ListItem>External behaviour</ListItem>
          <ListItem>Input
            <AppearList className="!text-xl !leading-tight">
              <ListItem>User interactions: browser events
                <UnorderedList className="!text-xl !leading-tight">
                  <ListItem>Click, focus, input, etc.</ListItem>
                </UnorderedList>
              </ListItem>
              <ListItem>External events (mocked)
                <UnorderedList className="!text-xl !leading-tight">
                  <ListItem>Child component emits</ListItem>
                  <ListItem>Vuex updates, route changes</ListItem>
                  <ListItem>API responses</ListItem>
                  <ListItem>Component method calls?</ListItem>
                </UnorderedList>
              </ListItem>
            </AppearList>
          </ListItem>
          <ListItem>Output
            <AppearList className="!text-xl !leading-tight">
              <ListItem>HTML</ListItem>
              <ListItem>Component emits</ListItem>
              <ListItem>External interactions: API calls, Vuex updates (mocked)</ListItem>
            </AppearList>
          </ListItem>
        </AppearList>
      </SlideLayout.Center>
    </Deck>
  )
}

