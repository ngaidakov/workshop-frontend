import { ReactElement, ReactNode } from 'react'
import { Appear, Box, CodePane, Notes, Slide, UnorderedList, useSteps } from 'spectacle'
import { asTree, TreeObject } from 'treeify'
import { AnimatePresence, motion } from 'framer-motion'

export function TwoColumn ({
  id,
  left,
  right,
  notes,
  leftClass,
  rightClass,
  center = false,
  flipped = false,
  equal = false,
}: {
  id?: string
  left?: ReactElement
  right?: ReactElement
  notes?: ReactElement
  leftClass?: string
  rightClass?: string
  center?: boolean | 'left' | 'right'
  flipped?: boolean
  equal?: boolean
}) {
  const centerLeft = center === true || center === 'left' ? 'flex flex-col items-center justify-center' : ''
  const centerRight = center === true || center === 'right' ? 'flex flex-col items-center justify-center' : ''

  return <Slide id={id} className="[&>*]:!p-0 [&>*]:!flex-row">
    <Box className={`p-6 ${equal ? 'flex-1' : 'flex-0'} space-y-6 ${flipped ? '' : 'bg-neutral-200'} ${centerLeft} ${leftClass}`}>{left}</Box>
    <Box className={`p-6 flex-1 space-y-6 ${flipped ? 'bg-neutral-200' : ''} ${centerRight} ${rightClass}`}>{right}</Box>
    {notes ? <Notes>{notes}</Notes> : null}
  </Slide>
}

export function AppearReplace ({ children, init = false }: { children: ReactNode[], init?: boolean }) {
  const { step, isActive, placeholder } = useSteps(children.length - (init ? 1 : 0))

  return <>
    {placeholder}
    <AnimatePresence mode="wait">
      <motion.div
        key={step}
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
        transition={{ duration: 0.15 }}
      >
        {isActive || init ? children[step + (init ? 1 : 0)] : null}
      </motion.div>
    </AnimatePresence>
  </>
}

type CodeTextSize = 'md' | 'sm' | 'xs'
type HighlightRange = number[] | number[][]

export function Code ({ children, lang = 'ts', className, text = 'md', highlight }: {
  children: string,
  className?: string,
  lang?: string
  text?: CodeTextSize
  highlight?: HighlightRange
}) {
  const fontSize = (() => {
    switch (text) {
      case 'md': return '[&_pre]:!text-lg'
      case 'sm': return '[&_pre]:!text-sm'
      case 'xs': return '[&_pre]:!text-2xs'
    }
  })()

  return <Box
    className={`rounded-lg overflow-hidden max-w-max [&_code]:!font-mono ${fontSize} ${className}`}
  >
    <CodePane
      language={lang}
      showLineNumbers={false}
      highlightRanges={highlight}
    >{children}</CodePane>
  </Box>
}

export function Tree ({ children, text, highlight, lang }: {
  children: TreeObject
  text?: CodeTextSize
  highlight?: HighlightRange
  lang?: string
}) {
  return <Code
    className="[&_code]:!leading-none"
    text={text}
    highlight={highlight}
    lang={lang}
  >{
    asTree(children, false, true)
  }</Code>
}

export function AppearList ({ children, className }: { children: ReactNode[], className?: string }) {
  return <UnorderedList className={className}>{
    children.map((child, i) => <Appear key={i}>{child}</Appear>)
  }</UnorderedList>
}
