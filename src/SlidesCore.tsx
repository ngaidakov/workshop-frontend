import {
  Appear, Box,
  Deck, FullScreen,
  Heading,
  Image, ListItem, Notes, Slide,
  SlideLayout, SpectacleThemeOverrides, UnorderedList, useSteps,
} from 'spectacle'
import twConfig from '../tailwind.config.ts'
import resolveConfig from 'tailwindcss/resolveConfig'
import { AppearReplace, Code, Tree, TwoColumn } from './slideUtils.tsx'
import { config, dashboard, earthworks } from './routes.ts'
import { AnimatePresence, motion } from 'framer-motion'

import imgCollageV1 from './assets/collage-v1.png'
import imgCollageV2 from './assets/collage-v2.png'
import imgThemeCycle from './assets/theme-cycle.gif'
import imgThemeUsers from './assets/blue-theme-users.png'
import imgThemeTemplates from './assets/blue-theme-templates.png'
import imgV2Components from './assets/v2-components-dir.png'
import imgTooltipV1 from './assets/tooltip-v1.png'
import imgTooltipV2 from './assets/tooltip-v2.png'
import imgConfigComponents from './assets/should-be-core.png'
import imgVueDevtoolsTimeline from './assets/vue-devtools-timeline.png'
import imgToast from './assets/toast.png'
import imgFrontendDev from './assets/dev-myself.jpeg'

const tw = resolveConfig(twConfig).theme
const colors = tw.colors

const theme: SpectacleThemeOverrides = {
  fonts: {
    header: tw.fontFamily.body,
    text: tw.fontFamily.body,
    monospace: tw.fontFamily.mono,
  },
  colors: {
    primary: colors.neutral['300'],
    secondary: colors.yellow['500'],
    tertiary: colors.neutral['800'],
    quaternary: colors.neutral['100'],
    quinary: colors.yellow['600'],
  },
  fontSizes: {
    monospace: '1.5rem',
  },
}

function DesignSystemSlide () {
  const { step, placeholder } = useSteps(3)
  const fade = {
    initial: { opacity: 0 },
    animate: { opacity: 1 },
    transition: { duration: 0.2 },
  }

  return <>
    <Box className="flex-1 flex flex-col items-center justify-center w-1/2 relative border-r-8 border-neutral-800">
      {placeholder}
      <AnimatePresence>{step < 1 ? null :
        <motion.div {...fade} className="absolute inset-0 bg-neutral-800/90 w-full h-full justify-center py-6 px-8">
          <Heading className="!m-0 !text-yellow-300">The Andrew</Heading>

          {placeholder}
          <AnimatePresence>{ step < 2 ? null :
            <motion.div {...fade}>
              <UnorderedList className="list-disc">
                <ListItem>Tailwind</ListItem>
                <ListItem>New core components</ListItem>
                <ListItem>Theme colours</ListItem>
                <ListItem>More Vue Router</ListItem>
                <ListItem>Less Vuex</ListItem>
                <ListItem>New iAPI patterns</ListItem>
              </UnorderedList>
            </motion.div>
          }</AnimatePresence>
        </motion.div>
      }</AnimatePresence>

      <Image src={imgCollageV1} alt="V1" className="object-cover w-full h-full !m-0" />
    </Box>
    <Box className="flex-1 flex flex-col items-center justify-center w-1/2 relative">
      {placeholder}
      <AnimatePresence>{step !== 0 ? null :
        <motion.div {...fade} className="absolute inset-0 py-6 bg-neutral-800/90 w-full h-full justify-center">
          <Heading className="!m-0 !text-yellow-300">The Paul</Heading>
        </motion.div>
      }</AnimatePresence>

      <Image src={imgCollageV2} alt="V2" className="object-cover w-full h-full" />
    </Box>
  </>
}

export default function SlidesCore () {
  return (
    <Deck theme={theme} template={<Box className="absolute left-8 bottom-8 cursor-pointer"><FullScreen /></Box>}>
      <SlideLayout.Statement>
        Frontend Workshop
        <Notes><ul>
          <li>PRESS RECORD</li>
          <li>The plan: me on ..., Liam on Tailwind (couldn't come in), etc.</li>
          <li>Ask questions any time</li>
        </ul></Notes>
      </SlideLayout.Statement>

      <Slide id="design-systems" className="[&>*]:!p-0 [&>*]:!flex-row">
        <DesignSystemSlide />
        <Notes><ul>
          <li>Appear:
            <ul>
              <li>The Paul</li>
              <li>The Andrew</li>
              <li>Bullet points</li>
            </ul>
          </li>
        </ul></Notes>
      </Slide>

      <TwoColumn
        id="v2-components"
        center
        left={<Heading>Core Components</Heading>}
        right={<Appear className="h-full">
          <Image src={imgV2Components} alt="v2 components" height="100%" />
        </Appear>}
        notes={<ul>
          <li>Everything expected to be reused throughout the application</li>
          <li>They live in v2 because...</li>
          <li>Later I'll showcase some of these?</li>
        </ul>}
      />

      <Slide id="tooltip-v1/v2" className="bg-gradient-to-b from-neutral-800 from-40% to-40% to-neutral-200">
        <Box className="h-full grid grid-cols-2 gap-x-16 gap-y-32 content-center items-center place-items-start odd:[&>*]:justify-self-end">
          <Code>{`import { Tooltip } from '@components/core/text'`}</Code>
          <Code>{`import { Tooltip } from '@components/core/v2/informational'`}</Code>
          <Appear className="justify-self-end"><Image src={imgTooltipV1} /></Appear>
          <Appear><Image src={imgTooltipV2} /></Appear>
        </Box>
        <Appear className="absolute right-[65%] top-[27%]"><span className="text-[150px]">😱</span></Appear>
        <Notes><ul>
          <li>Appear:
            <ul>
              <li>v1 tooltip</li>
              <li>v2 tooltip</li>
              <li>scream</li>
            </ul>
          </li>
          <li>There's a bounty for v1 tooltips in Config</li>
        </ul></Notes>
      </Slide>

      <TwoColumn
        id="pseudo-core"
        flipped
        left={<Image src={imgConfigComponents} alt="components/modules/config" className="h-full mx-auto" />}
        leftClass="w-2/5"
        right={<>
          <Heading>Should be core</Heading>
          <Appear>
            <UnorderedList className="!text-neutral-800 list-disc !ml-64">
              <ListItem>Form</ListItem>
              <ListItem>Table</ListItem>
              <ListItem>ListView</ListItem>
              <ListItem>Tab?</ListItem>
            </UnorderedList>
          </Appear>
        </>}
        notes={<ul>
          <li>Appear:
            <ul>
              <li>Bullet list</li>
            </ul>
          </li>
        </ul>}
      />

      <TwoColumn
        id="theme-colours"
        flipped
        center
        left={<Heading className="!mx-0">Theme Colours</Heading>}
        right={<AppearReplace init>
          <Image src={imgThemeCycle} alt="Cycling through themes" className="object-contain" />

          <Code>{`
{
  theme: {
    colors: {
      primary: {
        50: 'var(--primary-color-50)',
        100: 'var(--primary-color-100)',
        200: 'var(--primary-color-200)',
        300: 'var(--primary-color-300)',
        400: 'var(--primary-color-400)',
        500: 'var(--primary-color-500)',
        600: 'var(--primary-color-600)',
        700: 'var(--primary-color-700)',
        800: 'var(--primary-color-800)'
      },
      yellow: {
        50: '#FEFCE8',
        100: '#FEF9C3',
        200: '#FEF08A',
        // ...
      },
    }
  }
}
          `}</Code>

          <Image src={imgThemeUsers} alt="Users list in blue theme" className="object-contain" />
          <Image src={imgThemeTemplates} alt="Templates list in blue theme" className="object-contain" />
        </AppearReplace>}
        rightClass="bg-neutral-100"
        notes={<ul>
          <li>Appear:
            <ul>
              <li>Tailwind config</li>
              <li>Users list (yellow badge)</li>
              <li>Email templates list (blue badges)</li>
            </ul>
          </li>
          <li>Here's me in blue theme</li>
        </ul>}
      />

      <TwoColumn
        id="vue-router-routes"
        center
        left={<Heading>Vue Router</Heading>}
        right={<AppearReplace>
          <Tree>{earthworks}</Tree>
          <Tree>{dashboard}</Tree>
          <Tree text="sm">{config}</Tree>
          <Box className="space-y-4">
            <Code text="sm">{`
export const drivers: RouteConfig<RouteName> = {
  path: 'drivers',
  name: RouteName.Drivers,
  component: DriverTab,
  children: [
    {
      path: ':id(\\d+)',
      name: RouteName.DriverEdit,
      component: DriverEditDrawer,
      props: ({ params }: Route) => ({ driverId: parseInt(params.id) })
    },
    {
      path: 'create',
      name: RouteName.DriverCreate,
      component: DriverCreateDrawer
    }
  ]
}
          `}</Code>

            <Tree lang="html" text="sm">{{
              '<config-screen>': {
                '<nav-tabs>': {},
                '<router-view>': {
                  '<driver-tab>': {
                    '<list-view>': {
                      '<table-view>': {},
                      '<router-view>': {
                        '<driver-edit-drawer>': {},
                      },
                    },
                  },
                },
              }
            }}</Tree>
          </Box>
        </AppearReplace>}
        notes={<ul>
          <li>Appear:
            <ul>
              <li>Earthworks (flat)</li>
              <li>Dashboard (tabs)</li>
              <li>Config</li>
              <li>Nested routes code</li>
              <li>Swipe to demo</li>
            </ul>
          </li>
          <li>As I said, we made significantly more use of it</li>
          <li>For Drawers: we wanted to allow linking to a specific driver, eg.</li>
          <li>Explain nested routes: router-views</li>
          <li>Demo Drawers</li>
          <li>I'd like to use query params for filters</li>
        </ul>}
      />

      <TwoColumn
        id="vue-router-names"
        center
        left={<Heading>Route Names</Heading>}
        right={<AppearReplace init>
          <Code text="xs">{`
export enum RouteName {
  Account = 'account',
  Profile = 'profile',
  Theme = 'theme',
  ApiKeys = 'apiKeys',
  ApiKeyCreate = 'apiKeyCreate',
  CancelCodes = 'cancelCodes',
  CancelCodeCreate = 'cancelCodeCreate',
  CancelCodeEdit = 'cancelCodeEdit',
  AdjustmentCodes = 'adjustmentCodes',
  AdjustmentCodeCreate = 'adjustmentCodeCreate',
  AdjustmentCodeEdit = 'adjustmentCodeEdit',
  Pods = 'pods',
  Depots = 'depots',
  DepotCreate = 'depotCreate',
  DepotEdit = 'depotEdit',
  Users = 'users',
  UserCreate = 'userCreate',
  UserEdit = 'userEdit',
  Drivers = 'drivers',
  DriverCreate = 'driverCreate',
  DriverEdit = 'driverEdit',
  // ...
  InternalNotifications = 'internalNotifications',
  NotificationList = 'notificationList',
  NotificationTemplates = 'notificationTemplates',
  Modules = 'modules',
  RouteManagement = 'routeManagement',
  FollowMyDriver = 'followMyDriver',
  FollowMyDriverFullScreen = 'followMyDriverFullScreen',
  NotificationEmailTemplates = 'notificationEmailTemplates',
  NotificationEmailTemplateCreate = 'notificationEmailTemplateCreate',
  NotificationEmailTemplateEdit = 'notificationEmailTemplateEdit',
  NotificationSmsTemplates = 'notificationSmsTemplates',
  NotificationSmsTemplateCreate = 'notificationSmsTemplateCreate',
  NotificationSmsTemplateEdit = 'notificationSmsTemplateEdit',
  NotificationEventEmail = 'notificationEventEmail',
  NotificationEventSms = 'notificationEventSms',
  NotificationEventEmailTemplateEdit = 'notificationEventEmailTemplateEdit',
  NotificationEventEmailTemplateCreate = 'notificationEventEmailTemplateCreate',
  NotificationEventSmsTemplateEdit = 'notificationEventSmsTemplateEdit',
  NotificationEventSmsTemplateCreate = 'notificationEventSmsTemplateCreate',
}
          `}</Code>

          <Box className="space-y-4">
            <Code text="sm">{`
export const drivers: RouteConfig<RouteName> = {
  path: 'drivers',
  name: RouteName.Drivers,
  component: DriverTab,
  children: [
    {
      path: ':id(\\d+)',
      name: RouteName.DriverEdit,
      component: DriverEditDrawer,
      props: ({ params }: Route) => ({ driverId: parseInt(params.id) })
    },
    {
      path: 'create',
      name: RouteName.DriverCreate,
      component: DriverCreateDrawer
    }
  ]
}
          `}</Code>

            <Code lang="html" text="sm">{`
<router-link
  :to="{
    name: RouteName.DriverEdit,
    params: { id: driverId.toString() }
  }"
>Edit Driver</router-link>
            `}</Code>
          </Box>
        </AppearReplace>}
        notes={<ul>
          <li>Appear:
            <ul>
              <li>Names enum</li>
              <li>Code example</li>
            </ul>
          </li>
          <li>More like ids</li>
          <li>We actually haven't made much use of linking to routes</li>
          <li>Might be good to combine into one huge enum</li>
        </ul>}
      />

      <TwoColumn
        id="vuex-prev"
        center
        left={<Heading>Vuex</Heading>}
        leftClass="w-2/5"
        right={<AppearReplace>
          <Code>{`
function state () {
  return {
    jobs: [],
    selectedJobId: null,
    unappliedFilters: {},
    appliedFilters: {},
    filtersList: [],
    jobsListFeedback: null,
    detailsFeedback: null,
    podLocationTolerance: null,
    displayVehicleSpeed: false,
    snapToRoads: false,
    fetchingJobs: false,
    scrollFeedback: null,
    jobTypes: null,
    nextJobId: null,
    nextJobDeliveryDueBy: null
  }
}
          `}</Code>

          <Box className="text-2xl space-y-6 border-l-4 border-yellow-300 pl-6">
            <ul className="list-disc ml-6 space-y-4">
              <li>Multiple views may depend on the same piece of state.</li>
              <li>Actions from different views may need to mutate the same piece of state.</li>
            </ul>

            <p>For problem one, passing props can be tedious for deeply nested components, and simply doesn't work for sibling components. For problem two, we often find ourselves resorting to solutions such as reaching for direct parent/child instance references or trying to mutate and synchronize multiple copies of the state via events. Both of these patterns are brittle and quickly lead to unmaintainable code.</p>
          </Box>

          <Box className="w-full !px-8">
            <Heading>Cons</Heading>
            <UnorderedList className="list-disc">
              <ListItem>A lot of wiring</ListItem>
              <ListItem>Harder to test</ListItem>
              <ListItem>Typescript</ListItem>
            </UnorderedList>
          </Box>

          <Code text="sm">{`
type FetchFunction = (
  sortColumn: string,
  sortOrder: Order,
  pagination: PaginationParams,
  search: string
) => PaginatedPromise<ApiEntity>

const ListView = Vue.extend({
  props: {
    fetch: {
      type: Function as PropType<FetchFunction>,
      required: true
    },
    patch: {
      type: Function as PropType<(id: number, params: object) => Promise<void>>,
      required: true
    },
  },
  data () {
    return {
      fetchState: fetchState.loading<ApiEntity[]>(),
      sortColumn: this.defaultSortColumn,
      sortOrder: Order.Ascending,
      search: ''
    }
  },
}
          `}</Code>

          <Image src={imgVueDevtoolsTimeline} alt="Vue Devtools timeline" className="h-[700px]" />

          <Code>{`
interface State {
  userRoles: Role[]
  depotFetchState: LazyFetchState<Depot[]>
  vehicleTypeFetchState: LazyFetchState<VehicleType[]>
  emailEditorFetchState: LazyFetchState<EmailEditorDependencies>
  notifications: Notification[]
  modalPopup: Popup|null
  userId: number|null
  unsavedChanges: Set<string>
  unsavedChangesCount: number
}
          `}</Code>

          <Image src={imgToast} alt="Toast notification" className="h-20" />

        </AppearReplace>}
        notes={<ul>
          <li>Appear:
            <ul>
              <li>Job Query state</li>
              <li>Vuex docs pro</li>
              <li>Cons</li>
              <li>ListView</li>
              <li>Vue Devtools</li>
              <li>Config state</li>
              <li>Toasts</li>
            </ul>
          </li>
        </ul>}
      />

      <TwoColumn
        id="iapi"
        center
        left={<Heading>iAPI</Heading>}
        leftClass="w-1/3"
        right={
          <UnorderedList className="list-disc !text-2xl !min-w-max">
            <ListItem>From <code className="bg-[]">interfaces.ts</code> to types in individual modules</ListItem>
            <ListItem>More detailed param types: Post, Patch, Includes</ListItem>
            <ListItem>Typesafety for includes</ListItem>
          </UnorderedList>
        }
      />

      <SlideLayout.Center>
        <Heading>Thank you!</Heading>
        <Image src={imgFrontendDev} alt="I'm something of a frontend developer myself" />
      </SlideLayout.Center>
    </Deck>
  )
}

