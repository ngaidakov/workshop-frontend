import React from 'react'
import ReactDOM from 'react-dom/client'
import SlidesCore from './SlidesCore.tsx'

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <SlidesCore />
  </React.StrictMode>,
)
