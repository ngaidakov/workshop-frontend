export const earthworks = {
  "'/earthworks/createBooking'": {},
  "'/earthworks/editBooking/id/:bookingId'": {},
  "'/earthworks/copyBooking/id/:bookingId'": {},
  "'/earthworks/listBookings'": {},
  "'/earthworks/loadAllocation'": {}
}

export const dashboard = {
  "'/dashboard'": {
    "'jobPerformance'": {},
    "'jobAnalysis'": {},
    "'outstandingJobs'": {},
    "'epodConversion'": {},
    "'driverCompliance'": {},
    "'vehicleUtilisation'": {},
    "'planningAdherence'": {}
  }
}

export const config = {
  "'/config'": {
    "''": {
      "'cancelCodes'": {
        "':id(\\d+)'": {},
        "'create'": {}
      },
      "'adjustmentCodes'": {
        "':id(\\d+)'": {},
        "'create'": {}
      }
    },
    "'depots'": {
      "':id(\\d+)'": {},
      "'create'": {}
    },
    "'users'": {
      "':id(\\d+)'": {},
      "'create'": {}
    },
    "'drivers'": {
      "':id(\\d+)'": {},
      "'create'": {}
    },
    "'notifications'": {
      "'internal'": {},
      "'external'": {
        "':eventType(job_created|job_run_started|job_completed)/email'": {
          "''": {},
          "'template/create'": {},
          "'template/:id(\\d+)'": {}
        },
        "':eventType(job_created|job_run_started|job_completed)/sms'": {
          "''": {},
          "'template/create'": {},
          "'template/:id(\\d+)'": {}
        }
      }
    },
    "'modules'": {
      "'routeManagement'": {},
      "'followMyDriver'": {}
    }
  },
  "'/config/modules/followMyDriver/fullScreen'": {}
}
