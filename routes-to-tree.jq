def routes:
  if . == null then {} else
  . | map(
    select(.redirect | not)
    | { ("\(.path)"): .children | routes }
  )
  | add
  end;

. | routes
