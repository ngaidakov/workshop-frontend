# Frontend Workshop

Slides for the Frontend Workshop delivered by Team Frunk.

Built with [Spectacle](https://formidable.com/open-source/spectacle/), a React-based presentation framework.

### Install

I used `pnpm`, though you can use `npm`:

```
pnpm i
```

### Run

```
pnpm start
```

Will bring up a Vite dev server.
